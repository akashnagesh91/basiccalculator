# Basic Calculator #

This project is a basic implementation of a calculator. It is a command line utility that takes in expressions to evaluate and prints the result to the console.


Example: 
add(1, 2) => 3
add(1, mult(2, 3)) => 7
mult(add(2, 2), div(9, 3)) => 12
let(a, 5, add(a, a)) => 10
let(a, 5, let(b, mult(a, 10), add(b, a))) => 55
let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b)) => 40

An expression is one of the of the following:

•	Numbers: integers between Integer.MIN_VALUE and Integer.MAX_VALUE

•	Variables: strings of characters, where each character is one of a-z, A-Z

•	Arithmetic functions: add, sub, mult, div, each taking two arbitrary expressions as arguments.  In other words, each argument may be any of the expressions on this list.

•	A “let” operator for assigning values to variables:
	let(<variable name>, <value expression>, <expression where variable is used>)

### How do I get set up? ###

Step 1: 

		git clone https://akashnagesh91@bitbucket.org/akashnagesh91/basiccalculator.git
Step 2:

		mvn clean install
Step 3:

		java -cp target/Calculator-jar-with-dependencies.jar synopsys.calculator.CalculatorMain

###Assumptions###

1) I am assuming default logging level to debug
2) The logger output is directed to the console on purpose. This can be changed in log4j.properties file.
3) I have used pipelines by bitbucket for CI.

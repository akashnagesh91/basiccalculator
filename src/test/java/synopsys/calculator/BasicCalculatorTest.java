package synopsys.calculator;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import synopsys.calculator.exception.UnsolvableExpressionException;

/**
 * Test class to check the functionality of {@code BasicCalculator}
 * 
 * @author akashnagesh
 *
 */
public class BasicCalculatorTest {

	private BasicCalculator basicCalculator;

	@Before
	public void initialize() {
		basicCalculator = new BasicCalculatorImpl();
	}

	@Test(expected = UnsolvableExpressionException.class)
	public void testingExpressionWithMissingParenthesis() throws UnsolvableExpressionException {
		basicCalculator.evaluate("add(1,2");
	}

	@Test(expected = UnsolvableExpressionException.class)
	public void testingExpressionMissingOperator() throws UnsolvableExpressionException {
		basicCalculator.evaluate("add(1,(2,3))");
	}

	@Test(expected = UnsolvableExpressionException.class)
	public void testingExpressionHavingStringInPlaceOfNumber() throws UnsolvableExpressionException {
		basicCalculator.evaluate("add(1,b)");
	}

	@Test
	public void givenACorrectExpressionShouldReturnCorrectResult() throws UnsolvableExpressionException {
		double result = basicCalculator.evaluate("let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b)))");
		Assert.assertEquals(40.0, result);
	}

	@Test
	public void addingMaxIntValuesShouldNotOverflow() throws UnsolvableExpressionException {
		double result = basicCalculator.evaluate("add(2147483647,2147483647)");
		Assert.assertEquals(4.294967294E9, result);
	}

}

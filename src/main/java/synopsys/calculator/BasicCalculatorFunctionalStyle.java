package synopsys.calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import synopsys.calculator.exception.UnsolvableExpressionException;

public class BasicCalculatorFunctionalStyle implements BasicCalculator {

	final static Map<String, Double> map = new HashMap<>();

	@Override
	public double evaluate(String expression) throws UnsolvableExpressionException {
		// Should add logic to compile the given string and execute it during
		// runtime.
		// We could use the Compiler API for this.
		return 0;
	}

	public static void main(String[] args) throws Exception {
		// a working example of how the given string can be executed to perform
		// the calculation
		System.out.println(apply(let("a", let("b", 10, add("b", "b")), let("b", 20, add("a", "b")))));
	}

	/**
	 * The add function takes two {@code Object} that could be any combination
	 * of an {@code Integer} or {@code String} or {@link Function}
	 * 
	 * @param x
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @param y
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @return {@link Function} which returns a double value after adding the
	 *         two operands
	 */
	@SuppressWarnings("unchecked")
	private static Function<Void, Double> add(Object x, Object y) {
		if (x instanceof Integer) {
			if (y instanceof Integer)
				return t -> (double) (int) x + (double) (int) y;
			else if (y instanceof String)
				return t -> (int) x + map.get(y);
			else
				return t -> (int) x + ((Function<Void, Double>) y).apply(null);
		} else if (x instanceof String) {
			if (y instanceof Integer)
				return t -> map.get(x) + (int) y;
			else if (y instanceof String)
				return t -> map.get(x) + map.get(y);
			else
				return t -> map.get(x) + ((Function<Void, Double>) y).apply(null);
		} else {
			if (y instanceof Integer)
				return t -> ((Function<Void, Double>) x).apply(null) + (int) y;
			else if (y instanceof String)
				return t -> ((Function<Void, Double>) x).apply(null) + map.get(y);
			else
				return t -> ((Function<Void, Double>) x).apply(null) + ((Function<Void, Double>) y).apply(null);
		}
	}

	/**
	 * The sub function takes two {@code Object} that could be any combination
	 * of an {@code Integer} or {@code String} or {@link Function}
	 * 
	 * @param x
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @param y
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @return {@link Function} which returns a double value after subtracting y
	 *         from x
	 * 
	 */
	@SuppressWarnings("unchecked")
	private static Function<Void, Double> sub(Object x, Object y) {
		if (x instanceof Integer) {
			if (y instanceof Integer)
				return t -> (double) (int) x - (double) (int) y;
			else if (y instanceof String)
				return t -> (int) x - map.get(y);
			else
				return t -> (int) x - ((Function<Void, Double>) y).apply(null);
		} else if (x instanceof String) {
			if (y instanceof Integer)
				return t -> map.get(x) - (int) y;
			else if (y instanceof String)
				return t -> map.get(x) - map.get(y);
			else
				return t -> map.get(x) - ((Function<Void, Double>) y).apply(null);
		} else {
			if (y instanceof Integer)
				return t -> ((Function<Void, Double>) x).apply(null) - (int) y;
			else if (y instanceof String)
				return t -> ((Function<Void, Double>) x).apply(null) - map.get(y);
			else
				return t -> ((Function<Void, Double>) x).apply(null) - ((Function<Void, Double>) y).apply(null);
		}
	}

	/**
	 * The mult function takes two {@code Object} that could be any combination
	 * of an {@code Integer} or {@code String} or {@link Function}
	 * 
	 * @param x
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @param y
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @return {@link Function} which returns a double value after multiplying
	 *         the two operands x and y
	 */
	@SuppressWarnings("unchecked")
	private static Function<Void, Double> mult(Object x, Object y) {
		if (x instanceof Integer) {
			if (y instanceof Integer)
				return t -> (double) (int) x * (double) (int) y;
			else if (y instanceof String)
				return t -> (int) x * map.get(y);
			else
				return t -> (int) x * ((Function<Void, Double>) y).apply(null);
		} else if (x instanceof String) {
			if (y instanceof Integer)
				return t -> map.get(x) * (int) y;
			else if (y instanceof String)
				return t -> map.get(x) * map.get(y);
			else
				return t -> map.get(x) * ((Function<Void, Double>) y).apply(null);
		} else {
			if (y instanceof Integer)
				return t -> ((Function<Void, Double>) x).apply(null) * (int) y;
			else if (y instanceof String)
				return t -> ((Function<Void, Double>) x).apply(null) * map.get(y);
			else
				return t -> ((Function<Void, Double>) x).apply(null) * ((Function<Void, Double>) y).apply(null);
		}
	}

	/**
	 * The div function takes two {@code Object} that could be any combination
	 * of an {@code Integer} or {@code String} or {@link Function}
	 * 
	 * @param x
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @param y
	 *            operand which is either a {@code Integer} or {@code String} or
	 *            {@link Function}
	 * @return {@link Function} which returns a double value after dividing the
	 *         two operands
	 */
	@SuppressWarnings("unchecked")
	private static Function<Void, Double> div(Object x, Object y) {
		if (x instanceof Integer) {
			if (y instanceof Integer)
				return t -> (double) (int) x / (double) (int) y;
			else if (y instanceof String)
				return t -> (int) x / map.get(y);
			else
				return t -> (int) x / ((Function<Void, Double>) y).apply(null);
		} else if (x instanceof String) {
			if (y instanceof Integer)
				return t -> map.get(x) / (int) y;
			else if (y instanceof String)
				return t -> map.get(x) / map.get(y);
			else
				return t -> map.get(x) / ((Function<Void, Double>) y).apply(null);
		} else {
			if (y instanceof Integer)
				return t -> ((Function<Void, Double>) x).apply(null) / (int) y;
			else if (y instanceof String)
				return t -> ((Function<Void, Double>) x).apply(null) / map.get(y);
			else
				return t -> ((Function<Void, Double>) x).apply(null) / ((Function<Void, Double>) y).apply(null);
		}
	}

	private static Function<Void, Double> let(String variable, double val, Function<Void, Double> fn) {
		return (t) -> {
			map.put(variable, val);
			return fn.apply(null);
		};
	}

	private static Function<Void, Double> let(String variable, Function<Void, Double> fn1, Function<Void, Double> fn2) {
		return (t) -> {
			map.put(variable, fn1.apply(null));
			return fn2.apply(null);
		};
	}

	private static double apply(Function<Void, Double> fn) {
		return fn.apply(null);
	}
}

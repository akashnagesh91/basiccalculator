package synopsys.calculator.exception;

/**
 * Thrown to indicate that the given expression is unsolvable. This is a wrapper
 * {@code Exception} that wraps all checked and un-checked exceptions.
 * 
 * @author akashnagesh
 *
 */
public class UnsolvableExpressionException extends Exception {
	/**
	 * Constructs an {@code UnsolvableExpressionException} with no detail
	 * message
	 */
	public UnsolvableExpressionException() {

	}

	/**
	 * Constructs an {@code UnsolvableExpressionException} with the specified
	 * cause
	 * 
	 * @param cause
	 *            cause that resulted in this exception
	 */
	public UnsolvableExpressionException(Throwable cause) {
		super(cause);
	}

	/**
	 * Constructs an {@code UnsolvableExpressionException} with the specified
	 * detail message
	 * 
	 * @param message
	 *            the detail message
	 */
	public UnsolvableExpressionException(String message) {
		super(message);
	}

	/**
	 * Constructs an {@code UnsolvableExpressionException} with the specified
	 * detail message and cause
	 * 
	 * @param message
	 *            the detail message
	 * @param cause
	 *            cause that resulted in this exception
	 */
	public UnsolvableExpressionException(String message, Throwable cause) {
		super(message, cause);
	}
}

package synopsys.calculator;

import synopsys.calculator.exception.UnsolvableExpressionException;

/**
 * A basic calculator capable of performing addition, subtraction,
 * multiplication, division
 * 
 * @author akashnagesh
 *
 */
public interface BasicCalculator {
	/**
	 * Evaluates the given expression
	 * 
	 * @param expression
	 *            expression to solve
	 * @return the value of the solved expression
	 * @throws UnsolvableExpressionException
	 *             which is a wrapper for all the checked and un-checked
	 *             exceptions encountered during the execution of this method
	 */
	public double evaluate(String expression) throws UnsolvableExpressionException;
}

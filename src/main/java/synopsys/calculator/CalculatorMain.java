package synopsys.calculator;

import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import synopsys.calculator.exception.UnsolvableExpressionException;

/**
 * Starting point for {@link BasicCalculator}
 * 
 * @author akashnagesh
 *
 */
public class CalculatorMain {

	private final static Logger logger = Logger.getLogger(CalculatorMain.class);

	public static void main(String[] args) {

		final Scanner scanner = new Scanner(new InputStreamReader(System.in));
		System.out.println("Please enter verbosity level for the logger or press enter for default log level");
		System.out.println("Logger verbosity levels could be INFO, DEBUG, WARN or ERROR");
		final String logLevel = scanner.nextLine().toUpperCase();
		switch (logLevel) {
		case "INFO":
			LogManager.getRootLogger().setLevel(Level.INFO);
			logger.info("Logger level set to INFO");
			break;
		case "DEBUG":
			LogManager.getRootLogger().setLevel(Level.DEBUG);
			logger.debug("Logger level set to DEBUG");
			break;
		case "WARN":
			LogManager.getRootLogger().setLevel(Level.WARN);
			logger.warn("Logger level set to WARN");
			break;
		case "ERROR":
			LogManager.getRootLogger().setLevel(Level.ERROR);
			logger.error("Logger level set to ERROR");
			break;
		default:
			logger.debug("Logger level set to debug");
		}

		final BasicCalculator basicCalculator = new BasicCalculatorImpl();
		while (true) {
			System.out.println();
			System.out.println("Please enter the expression to be evaluated or \"quit\" to quit");
			final String expression = scanner.nextLine();

			if (expression.equalsIgnoreCase("quit")) {
				logger.info("Exiting calculator");
				scanner.close();
				System.exit(0);
			}
			try {
				double result = basicCalculator.evaluate(expression);
				final StringBuilder sb = new StringBuilder();
				sb.append("Result for the evaluated expression ");
				sb.append(expression);
				sb.append(" is ");
				sb.append(System.lineSeparator());
				sb.append(result);
				System.out.println(sb);
			} catch (UnsolvableExpressionException e) {
				System.err.println("Something went wrong. Please try again");
				logger.error(e.getMessage(), e);
			}
		}
	}

}

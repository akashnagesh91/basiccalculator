package synopsys.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import synopsys.calculator.exception.UnsolvableExpressionException;

public class BasicCalculatorImpl implements BasicCalculator {

	public static final String ADD = "add", SUB = "sub", MULT = "mult", DIV = "div", LET = "let", ROOT = "root";

	private static Logger logger = Logger.getLogger(BasicCalculatorImpl.class);

	@Override
	public double evaluate(String expression) throws UnsolvableExpressionException {
		logger.info("Starting the evaluation of expression " + expression);
		if (!isValidExpression(expression)) {
			logger.error("The expression could be missing a parantheses");
			throw new UnsolvableExpressionException(
					"The expression is syntactically incorrect : the expression could be missing a parentheses");
		}
		double result = 0;
		try {
			final Object parsedExpression = parseExpression(expression.replaceAll(" ", StringUtils.EMPTY),
					BasicCalculatorImpl.ROOT, 0)[0];
			result = evaluate(parsedExpression, new HashMap<>());
		} catch (ArrayIndexOutOfBoundsException e) {
			logger.warn("Expression is not well formed", e);
			throw new UnsolvableExpressionException(e);
		} catch (NumberFormatException e) {
			logger.warn(e);
			throw new UnsolvableExpressionException(e);
		} catch (ArithmeticException e) {
			logger.warn(e);
			throw new UnsolvableExpressionException(e);
		} catch (Exception e) {
			logger.warn(e);
			throw new UnsolvableExpressionException(e);
		}
		return result;
	}

	/**
	 * Evaluates the given parsed expression.
	 * 
	 * @param expression
	 *            expression to evaluate
	 * @param variablesMap
	 *            map used as a dictionary to store variable names
	 * @return a double value obtained from evaluating the given expression
	 * @throws UnsolvableExpressionException
	 *             in case the expression is not well formed
	 */
	private double evaluate(final Object expression, final Map<Object, Double> variablesMap)
			throws UnsolvableExpressionException {
		if (expression instanceof String) {
			if (NumberUtils.isCreatable((String) expression)) {
				return Integer.parseInt(expression.toString());
			} else {
				return variablesMap.get(expression);
			}
		} else {
			final List<?> expressionArray = (ArrayList<?>) expression;
			final String operator = expressionArray.get(0).toString();
			final Object expressionToEvaluate = expressionArray.get(1);
			if (operator.equalsIgnoreCase(BasicCalculatorImpl.ROOT)) {
				return evaluate(expressionToEvaluate, variablesMap);
			} else if (operator.equalsIgnoreCase(BasicCalculatorImpl.ADD)) {
				logger.debug("Adding " + expressionToEvaluate + " " + expressionArray.get(2));
				return evaluate(expressionToEvaluate, variablesMap) + evaluate(expressionArray.get(2), variablesMap);
			} else if (operator.equalsIgnoreCase(BasicCalculatorImpl.SUB)) {
				logger.debug("Subtracting " + expressionToEvaluate + " from " + expressionArray.get(2));
				return evaluate(expressionToEvaluate, variablesMap) - evaluate(expressionArray.get(2), variablesMap);
			} else if (operator.equalsIgnoreCase(BasicCalculatorImpl.MULT)) {
				logger.debug("Multiplying " + expressionToEvaluate + " with " + expressionArray.get(2));
				return evaluate(expressionToEvaluate, variablesMap) * evaluate(expressionArray.get(2), variablesMap);
			} else if (operator.equalsIgnoreCase(BasicCalculatorImpl.DIV)) {
				logger.debug("Dividing " + expressionToEvaluate + " by " + expressionArray.get(2));
				return evaluate(expressionToEvaluate, variablesMap) / evaluate(expressionArray.get(2), variablesMap);
			} else if (operator.equalsIgnoreCase(BasicCalculatorImpl.LET)) {
				logger.debug("Encountered let, adding the variable " + expressionToEvaluate + " to map");
				variablesMap.put(expressionToEvaluate, evaluate(expressionArray.get(2), variablesMap));
				return evaluate(expressionArray.get(3), variablesMap);
			} else {
				logger.warn("Missing operation");
				throw new UnsolvableExpressionException("The expression is syntactically incorrect");
			}
		}
	}

	/**
	 * This method returns a nested array of {@code Object} after parsing the
	 * given expression string.
	 * 
	 * For example expression "add(1,2)" results in [root,[add,1,2]]
	 * 
	 * @param expression
	 *            the string expression to be parssed
	 * @param function
	 *            the function that should be evaluated
	 * @param index
	 *            current index
	 * @return an nested array of Object as explained above
	 */
	private Object[] parseExpression(final String expression, final String function, int index) {
		final List<Object> expressionList = new ArrayList<>();
		expressionList.add(function);
		final StringBuilder sb = new StringBuilder();

		while (true) {
			if (expression.charAt(index) == ')') {
				if (sb.length() != 0)
					expressionList.add(sb.toString());
				break;
			} else if (expression.charAt(index) == '(') {
				Object[] subExpression = parseExpression(expression, sb.toString(), index + 1);
				index = (int) subExpression[1];
				expressionList.add(subExpression[0]);
				sb.setLength(0);
				if (function.equals(BasicCalculatorImpl.ROOT))
					break;
			} else if (expression.charAt(index) == ',') {
				if (sb.length() != 0)
					expressionList.add(sb.toString());
				sb.setLength(0);
			} else {
				sb.append(expression.charAt(index));
			}
			index++;
		}
		return new Object[] { expressionList, index };
	}

	/**
	 * This method returns true the expression is well formed
	 * 
	 * @param expression
	 *            The expression to be validated
	 * @return true if the expression is well formed else false
	 */
	private boolean isValidExpression(final String expression) {
		final char[] expressionArray = expression.toCharArray();
		final Stack<Character> parenthesis = new Stack<>();
		for (int i = 0; i < expressionArray.length; i++) {
			if (expressionArray[i] == '(') {
				parenthesis.push('(');
			} else if (expressionArray[i] == ')') {
				if (parenthesis.isEmpty()) {
					return false;
				}
				parenthesis.pop();
			}
		}
		return parenthesis.isEmpty();
	}
}
